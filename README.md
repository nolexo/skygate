# Skygate

Project of REST API Game Skygate

### Installation

clone repository:

Docker installation:

```sh
$ docker-compose up
```

OR normal installation

```sh
$  pip install -r requirements.txt
```

### Run only for normal installation

```sh
$ python game.py
```

### Api requests

Request sample GET : http://127.0.0.1:5000/ranks

Client-ID for user = asdf, Grandmaster = abcd

| HTTP request|Method| Headers | params | RETURN | EXAMPLE |
| ------ | ------ | ------ | ------ | ------ | ------ |
| http://127.0.0.1:5000/deaths | GET| ('Client-ID': 'xxxTOKEN') ||json list of death users | {"deaths":[{"date":"2018-06-19 16:54:56","username":"sulat","win":0}]} |
| http://127.0.0.1:5000/fights | GET|('Client-ID': 'xxxTOKEN')|ids='unique ids of tournament string'|run random fight and return json list winners and losers | {"users":[{"alive":true,"id":10,"loses":1,"race":"xanat","username":"sulastss","wins":4}]} |
| http://127.0.0.1:5000/hero | GET|('Client-ID': 'xxxTOKEN')||json list of user | {"users": [{"id": 1, "username": "doonky", "race": "mooner", "alive": 1, "win": 1, "lose": 3, "admin": 0}]} |
| http://127.0.0.1:5000/tournament | GET|('Client-ID': 'xxxTOKEN')||json list users who participate in the tournament | {"torn": [{"userFirst": "doonky", "userSecond": "rump"}, {"userFirst": "sulat", "userSecond": "trixat"}, {"userFirst": "doonky", "userSecond": "sulat"}]} |
| http://127.0.0.1:5000/tournament | POST|('Client-ID': 'xxxTOKEN')|first='first username from db', second='second username', ids=set unique id torunament| add heroes to torunament | {"status": 'user add to tournament'} | 
| http://127.0.0.1:5000/tournament | DELETE|('Client-ID': 'xxxTOKEN')|id='id tournament fight'| removing players from the tournament | {"status": 'match was delete'} |
| http://127.0.0.1:5000/ranks | GET|||json list of ranking all alive users | {"ranks": [{"username": "trixat", "win": 5, "lose": 0}, {"username": "coquar22", "win": 4, "lose": 2}]} |
| http://127.0.0.1:5000/winners | POST|('Client-ID': 'xxxTOKEN')|winer=username, loser=username, ids=unique id of tournament|set winners & losers  | {"status": 'winner is name of winner} |
| http://127.0.0.1:5000/winners | GET|('Client-ID': 'xxxTOKEN')|ids=unique ids for torunament|json list of winners in torunament | {"winners": [{"ids": "1f3","loser": "kindey", "winner": "doonky"}]|