from flask import Flask, request, jsonify, Blueprint
from flask_restful import Resource, Api
from sqlalchemy import create_engine
import battle
import os
import sqlite3
import re


db_connect = create_engine('sqlite:///users.db')
app = Flask(__name__)
app.secret_key = os.urandom(24)

api_bp = Blueprint('api', __name__)
api = Api(api_bp)

friend = {'mooner': 'deller', 'deller': 'mooner',
          'xanat': 'nuzgat', 'nuzgat': 'nuzgat'}


def regexp(expr, item):
    reg = re.compile(expr)
    return reg.search(item) is not None


def status(stat):
    return {'status': stat}


def go_grave(user_id):
    conn = db_connect.connect()
    conn.execute("insert or replace into grave values('{id}', datetime('now', 'localtime'))".format(id=user_id))


def check_race(id_first, id_second):
    conn = db_connect.connect()
    query = conn.execute("select race from hero where id = '{id}'".format(id=id_first))
    first_race = query.cursor.fetchone()[0]
    query = conn.execute("select race from hero where id = '{id}'".format(id=id_second))
    second_race = query.cursor.fetchone()[0]
    if first_race == second_race or id_first == id_second or friend[first_race] == second_race:
        return True
    else:
        return False


class Hero(Resource):

    @staticmethod
    def get():
        if not check_access(request.headers.get('Client-ID')):
            return jsonify({'status': 'no Client-ID'})
        conn = db_connect.connect()
        query = conn.execute("select * from hero")
        return {'users': [dict(zip(tuple(query.keys()), i)) for i in query.cursor]}

    @staticmethod
    def post(self):

        if not check_access(request.headers.get('Client-ID')):
            return jsonify(status('required Client-ID'))

        conn = db_connect.connect()
        if request.json:

            username = request.json['Username']
            if username:
                query = conn.execute("select username from hero where username ="
                                     " '{username}'".format(username=username))
                for i in query.cursor:
                    if i:
                        return status('Username is used')
            else:
                return status('username error')
            race = self.race(request.json['Race'])
            if race:
                conn.execute("insert into hero values(null,'{username}',"
                             " '{race}', 1, 0, 0, 0)".format(username=username, race=race))
                return status('new hero add')
            else:
                return status('wrong choose of race')
        return {'status': 'wrong data required json'}

    @staticmethod
    def race(number):
        races = {1: 'mooner', 2: 'nuzgat',
                 3: 'xanat',  4: 'deller'}
        return races.get(number, '')


class Fights(Resource):

    @staticmethod
    def get():
        if not check_access(request.headers.get('Client-ID')):
            return jsonify({'status': 'required Client-ID'})
        unique_id = request.args.get('ids')
        if unique_id:
            conn = db_connect.connect()
            query = conn.execute("select id, username, race from hero where alive = '1'")
            result = {'users': [dict(zip(tuple(query.keys()), i)) for i in query.cursor]}
            if len(result['users']) > 1:
                start_battle = battle.War(result, db_connect, unique_id)
                result['users'] = start_battle.get_users()
            return jsonify(result)
        else:
            return jsonify({'status': 'ids required'})


class Login(Resource):

    @staticmethod
    def get():
        conn = db_connect.connect()
        query = conn.execute("select username, race, admin from hero where id ="
                             " (select id from login where username = '{}' and password ="
                             " '{}')".format(request.args.get('login'), request.args.get('password')))
        result = {'data': [dict(zip(tuple(query.keys()), i)) for i in query.cursor]}
        return jsonify(result)


class ListTorn(Resource):
    @staticmethod
    def get():
        if not check_access(request.headers.get('Client-ID')):
            return jsonify({'status': 'required Client-ID'})
        conn = db_connect.connect()
        query = conn.execute("select userFirst, userSecond, id from tornament")
        return {'torn': [dict(zip(tuple(query.keys()), i)) for i in query.cursor]}


class Rank(Resource):
    @staticmethod
    def get():
        conn = db_connect.connect()
        query = conn.execute("select username, win, lose from hero where alive = '1' order by win desc")
        return {'ranks': [dict(zip(tuple(query.keys()), i)) for i in query.cursor]}


class Deaths(Resource):
    @staticmethod
    def get():
        if not check_access(request.headers.get('Client-ID')):
            return jsonify({'status': 'required Client-ID'})
        conn = db_connect.connect()
        query = conn.execute("select hero.username, hero.win, grave.date from hero,"
                             " grave where hero.id = grave.id order by win desc")
        result = {'deaths': [dict(zip(tuple(query.keys()), i)) for i in query.cursor]}
        return jsonify(result)


def check_tornament(ids):
    conn = db_connect.connect()
    query = conn.execute("select id from tornament where ids = '{id}'".format(id=ids))
    row = query.cursor.fetchone()
    if row:
        return True
    else:
        return False


def add_tornament(ids, first_user, second_user):
    conn = db_connect.connect()
    conn.execute("insert or replace into tornament values(null, '{ids}',"
                 " (select username from hero where id = '{usF}'),"
                 " (select username from hero where id = '{usS}'))".format(ids=ids, usF=first_user, usS=second_user))


def get_user(username):
    conn = db_connect.connect()
    query = conn.execute("select id from hero where username = '{user}'".format(user=username))
    row = query.cursor.fetchone()
    if row:
        return row[0]
    else:
        return ''


def check_access(token):
    conn = db_connect.connect()
    query = conn.execute("select admin from hero where id = (select id from login where token = '{}')".format(token))
    row = query.cursor.fetchone()
    if row:
        if row[0] == 1:
            return True
        else:
            return False
    else:
        return False


def delete_tournament(id_tornament):
    conn = db_connect.connect()
    conn.execute("delete from tornament where id = '{id}'".format(id=id_tornament))


def set_db_row(query):
    conn = db_connect.connect()
    conn.execute(query)


def rege(ids):
    conn = sqlite3.connect('users.db')
    conn.create_function("REGEXP", 2, regexp)
    cursor = conn.cursor()
    query = cursor.execute("select winner, loser, ids FROM winners where ids regexp '^\d+{id}\d+'".format(id=ids))
    column_names = list(map(lambda x: x[0], cursor.description))
    return {'winners': [dict(zip(column_names, i)) for i in query]}


def get_winners(ids):
    conn = db_connect.connect()
    if ids:
        query = rege(ids)
    else:
        query = conn.execute("select winner, loser, ids from winners")
    return query


class Tournament(Resource):

    @staticmethod
    def post():

        if not check_access(request.headers.get('Client-ID')):
            return jsonify({'status': 'required Client-ID'})

        first_hero = request.args.get('first')
        second_hero = request.args.get('second')
        unique_id = request.args.get('ids')
        # get user id from db
        first_user_db_id = get_user(first_hero)
        second_user_db_id = get_user(second_hero)
        if first_user_db_id and second_user_db_id:
            # check if users are friends
            if not check_race(first_user_db_id, second_user_db_id):
                # create unique id to db
                if first_user_db_id > second_user_db_id:
                    ids = "{0}{1}{2}".format(second_user_db_id, unique_id, first_user_db_id)
                else:
                    ids = "{0}{1}{2}".format(first_user_db_id, unique_id, second_user_db_id)
                # check if users add to tournament
                if check_tornament(ids):
                    contex = status('users are in the tournament')
                else:
                    add_tornament(ids, first_user_db_id, second_user_db_id)
                    contex = status('user add to tournament')
            else:
                contex = status("Users are friends race can't fight")
        elif unique_id:
            contex = status('wrong username')
        else:
            contex = status('no specify unique ids')
        return jsonify(contex)

    @staticmethod
    def get():
        if not check_access(request.headers.get('Client-ID')):
            return jsonify(status('required Client-ID'))
        return ListTorn.get()

    # delete match
    @staticmethod
    def delete():
        if not check_access(request.headers.get('Client-ID')):
            return jsonify(status('required Client-ID'))
        id_tournament = request.args.get('id')
        contex = status('required Client-ID')
        if id_tournament:
            delete_tournament(id_tornament=id_tournament)
            contex = status('match was delete')
        return jsonify(contex)


class SetWinners(Resource):

    @staticmethod
    def post():
        if not check_access(request.headers.get('Client-ID')):
            return jsonify(status('required Client-ID'))
        winner_in_tournament = request.args.get('winner')
        loser_in_tournament = request.args.get('loser')
        unique_ids = request.args.get('ids')
        first_user_id = get_user(winner_in_tournament)
        contex = status('wrong users or ids')
        ids = ""
        second_user_id = get_user(loser_in_tournament)
        if first_user_id > second_user_id and unique_ids:
            ids = "{0}{1}{2}".format(second_user_id, unique_ids, first_user_id)
        elif first_user_id < second_user_id and unique_ids:
            ids = "{0}{1}{2}".format(first_user_id, unique_ids, second_user_id)
        elif not unique_ids:
            contex = status('no specified ids')
        else:
            contex = status('users are not in tournament')
        if check_tornament(ids=ids):
            set_db_row("insert or replace into winners values(null, '{ids}', '{win}', '{lose}')"
                       .format(ids=ids, win=winner_in_tournament, lose=loser_in_tournament))
            contex = status('winner is {}'.format(winner_in_tournament))
        return jsonify(contex)

    @staticmethod
    def get():
        if not check_access(request.headers.get('Client-ID')):
            return jsonify(status('required Client-ID'))

        unique_id_tournament = request.args.get('ids')
        if unique_id_tournament:
            return jsonify(get_winners(unique_id_tournament))
        else:
            query = get_winners(unique_id_tournament)
            return jsonify({'winners': [dict(zip(tuple(query.keys()), i)) for i in query.cursor]})


# @app.route("/")
# def index():
#     return render_template("index.html")


# transfer soon to api
# @app.route("/user", methods=['POST', 'GET'])
# def user():
#     if request.method == 'POST':
#         login = request.form.get('Login')
#         password = request.form.get('Password')
#         if login and password:
#             contex = requests.get("http://127.0.0.1:5000/login?login={}&password={}".format(login, password)).json()
#             if contex['data']:
#                 if contex['data'][0]['admin'] == 1:
#                     users = requests.get("http://127.0.0.1:5000/ranks".format(login, password)).json()
#                     session['username'] = login
#                     session['password'] = password
#                     session['admin'] = 1
#                     return render_template("admin.html", contex=contex['data'][0], users=users['ranks'])
#                 else:
#                     session['username'] = login
#                     session['password'] = password
#                     session['admin'] = 0
#                     return render_template("user.html", contex=contex['data'][0])
#     if request.method == 'GET':
#         if 'username' in session:
#
#             if session['admin'] == 1:
#                 contex = requests.get("http://127.0.0.1:5000/login?login={}&password={}"
#                                       .format(session['username'], session['password'])).json()
#                 users = requests.get("http://127.0.0.1:5000/ranks"
#                                      .format(session['username'], session['password'])).json()
#                 return render_template("admin.html", contex=contex['data'][0], users=users['ranks'])
#             else:
#                 contex = requests.get("http://127.0.0.1:5000/login?login={}&password={}"
#                                       .format(session['username'], session['password'])).json()
#                 return render_template("user.html", contex=contex['data'][0])
#     return redirect('/')


api.add_resource(Hero, '/hero')
api.add_resource(Fights, '/fights')
api.add_resource(ListTorn, '/torn')
api.add_resource(Rank, '/ranks')
api.add_resource(Deaths, '/deaths')
api.add_resource(Login, '/login')
api.add_resource(Tournament, '/tournament')
api.add_resource(SetWinners, '/winners')
app.register_blueprint(api_bp)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
