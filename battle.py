import random
friend = {'mooner': 'deller', 'deller': 'mooner',
          'xanat': 'nuzgat', 'nuzgat': 'xanat'}


def get_user(first_user, second_user, con):
    query = con.execute("select id, race, username from hero where username = '{}' or username = '{}'"
                        .format(first_user, second_user))
    return [dict(zip(tuple(query.keys()), i)) for i in query.cursor]


class War(object):
    def __init__(self, warriors, db, unique_ids):
        self.unique_ids = unique_ids
        self.users_to_tournament = warriors['users']
        self.fighters = []
        self.db_connect = db
        self.result_of_war = []
        self.go_home = []
        self.who_won_and_who_lose = []
        self.prepare()
        self.send_db_winners()

    def prepare(self):

        # create list fighters
        for war in self.users_to_tournament:
            self.fighters.append(Fighter(username=war['username'], race=war['race'], ids=war['id']))
        # set fighters all against all hero, player can't fight against this same opponent
        for x in self.fighters:
            fight = [c for c in self.fighters if x.race != c.race and friend[x.race]
                     != c.race and x not in c.fighters and c not in x.fighters]
            x.set_fighters(fight)
        while 1:
            for x in self.fighters:
                # fight began :) return list winner first element loser second
                winner_loser = x.fight()
                # if list is bigger than 1 set losers and winners to db, if not remove fighter
                if len(winner_loser) > 1:
                    self.fights(winner_loser[0], winner_loser[1])
                    pass
                else:
                    # go home stored a all users who participate in tournament
                    self.go_home.append(winner_loser[0])
                    self.fighters.remove(winner_loser[0])
            # if list is empty add result to variable and break loop
            if not self.fighters:
                for x in self.go_home:
                    self.result_of_war.append({'id': x.id, 'username': x.username, 'race': x.race,
                                              'alive': x.alive, 'wins': x.win, 'loses': x.lose})
                break

    # add to db who won who lose
    def fights(self, *args):
        set_players = [args[0], args[1]]
        first_user = set_players[0]
        second_user = set_players[1]
        while 1:
            if first_user.health <= 0:
                if self.kill(first_user.id):
                    first_user.alive = False
                    for x in self.fighters:
                        x.remove_fighters(first_user)
                self.who_won_and_who_lose.append({'winner': second_user.username, 'loser': first_user.username,
                                                  'ids': self.get_ids(first_user.id, second_user.id)})
                self.winner(second_user.id)
                second_user.reset_hp()
                first_user.reset_hp()
                break
            if second_user.health <= 0:
                if self.kill(second_user.id):
                    second_user.alive = False
                    for x in self.fighters:
                        x.remove_fighters(second_user)
                self.who_won_and_who_lose.append({'winner': first_user.username, 'loser': second_user.username,
                                                  'ids': self.get_ids(first_user.id, second_user.id)})
                self.winner(first_user.id)
                second_user.reset_hp()
                first_user.reset_hp()
                break

    def kill(self, user_id):
        con = self.db_connect.connect()
        if random.randrange(10) == 1:
            con.execute("update hero set alive = '0' where id = {id}".format(id=user_id))
            con.execute("insert or replace into grave values('{id}', datetime('now', 'localtime'))".format(id=user_id))
            con.execute("update hero set lose = lose + 1 where id = {id}".format(id=user_id))
            con.execute("select username from hero where id = {}".format(user_id))
            return True
        else:
            con.execute("update hero set lose = lose + 1 where id = {id}".format(id=user_id))
            return False

    def get_ids(self, first_id, second_id):
        if first_id > second_id:
            return '{0}{1}{2}'.format(second_id, self.unique_ids, first_id)
        else:
            return '{0}{1}{2}'.format(first_id, self.unique_ids, second_id)

    def winner(self, user_id):
        con = self.db_connect.connect()
        con.execute("update hero set win = win + 1 where id = {id}".format(id=user_id))

    # returns dict list detail about hero
    # {'id': id, 'username': 'user', 'race': race,'alive': true/false, 'wins': returns number of wins
    # on tournament, 'loses': same like wins but on lose}
    def get_users(self):
        return self.result_of_war

    # returns the result of the fight {'winner': 'user', 'loser': 'user', 'ids': 'unique ids'}
    def get_who_win(self):
        return self.who_won_and_who_lose

    def send_db_winners(self):
        for data in self.who_won_and_who_lose:
            con = self.db_connect.connect()
            con.execute("insert or replace into winners values(null, '{}', '{}', '{}')".format(
                data['ids'], data['winner'], data['loser']
            ))


class Tournament(War):
    def __init__(self, warrior, db, ids):
        warriors = {'users': warrior['torn']}
        super().__init__(warriors=warriors, db=db, unique_ids=ids)

    def prepare(self):
        # create fighters
        con = self.db_connect.connect()
        for hero in self.users_to_tournament:
            # get user return list dict of 2 heroes {'username': user, 'race': race, 'ids': 'id player'}
            hero_details = get_user(hero['userFirst'], hero['userSecond'], con)
            set_fighters = []
            for war in hero_details:
                set_fighters.append(Fighter(username=war['username'], race=war['race'], ids=war['id']))
            for x in set_fighters:
                fight = [c for c in set_fighters if x.race != c.race and friend[x.race]
                         != c.race and x not in c.fighters and c not in x.fighters]
                self.fighters.append(x)
                x.set_fighters(fight)
        while 1:
            for x in self.fighters:
                # fight began :) returns list winner first element loser second
                winner_loser = x.fight()
                # if list is bigger than 1 set losers and winners to db, if not remove fighter
                if len(winner_loser) > 1:
                    self.fights(winner_loser[0], winner_loser[1])
                    pass
                else:
                    self.go_home.append(winner_loser[0])
                    self.fighters.remove(winner_loser[0])
            # if list is empty add result to variable and break loop
            if not self.fighters:
                for x in self.go_home:
                    self.result_of_war.append({'id': x.id, 'username': x.username, 'race': x.race,
                                              'alive': x.alive, 'wins': x.win, 'loses': x.lose})
                break


class Fighter:
    def __init__(self, username, race, ids):
        self.fighters = []
        self.username = username
        self.race = race
        self.id = ids
        self.health = 25
        self.alive = True
        self.lose = 0
        self.win = 0

    def __str__(self):
        return self.username

    def attack(self):
        self.health -= random.randrange(5)

    def set_fighters(self, fight):
        self.fighters = fight

    def reset_hp(self):
        self.health = 25

    def remove_fighters(self, name):
        if name in self.fighters:
            self.fighters.remove(name)

    def fight(self):
        if self.fighters:
            oponent = random.choice(self.fighters)
            self.fighters.remove(oponent)
            # simple fight, random attack from 0-5 dmg who has less or equal health lose fight.
            while 1:
                self.attack()
                if self.health < 1:
                    self.lose += 1
                    oponent.win += 1
                    return [oponent, self]
                oponent.attack()
                if oponent.health < 1:
                    self.win += 1
                    oponent.lose += 1
                    return [self, oponent]
        else:
            return [self]
