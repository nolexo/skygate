FROM python:alpine

COPY . /skygate
WORKDIR /skygate

RUN apk update && apk add --no-cache && \
		pip install -r requirements.txt

CMD [ "python", "game.py" ]
